
def load_points(file_path):
    points = []
    with open(file_path) as file:
        for line in file.readlines()[1:]:
            words = line.strip().split(",")
            points.append({"lat": float(words[2]), "lon": float(words[1])})
    print(f"success load points: {len(points)}")
    return points

points = load_points("./lfs_practice/novosibirsk.csv")
print(points)

